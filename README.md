# Tracer

A simple and lightweight library for hierarchical measurement of durations in your Java code in your Java application, with added convenience for Spring Boot. Typically the results would be logged looking something like this:

```
2019-07-05 13:58:28.420 ==========================================
SomeService#doStuff('abc', 'def') * ...... [475 ms @ 13:58:28.420]
  Step 1 ................................. [ 51 ms @ 13:58:28.431]
  Step 2 ................................. [362 ms @ 13:58:28.482]
    SomeOtherService#doStuff(123, 456) * . [311 ms @ 13:58:28.533]
      Step A ............................. [101 ms @ 13:58:28.542]
      Step B ............................. [201 ms @ 13:58:28.643]
  Step 3 ................................. [ 51 ms @ 13:58:28.844]
```

This library can't and isn't intended to replace more sophisticated JVM profiler and monitoring solutions and provides no analysis of the logged traces. But it can offer a quick and easy way to e.g. keep an eye on the duration of certain potentially expensive service methods, even in production.

## Getting started

### Spring Boot

Tracer offers auto configuration with Spring Boot after the following dependency has been added to your project:

```
  <dependency>
    <groupId>de.codecamp.tracer</groupId>
    <artifactId>tracer-spring-boot-starter</artifactId>
    <version>1.0.2-SNAPSHOT</version>
  </dependency>
```

Only the associated logger `de.codecamp.tracer` needs to be set to the TRACE level manually:

```
logging.level.de.codecamp.tracer = TRACE
```

All configuration properties can be found below the ´codecamp.tracer.´ prefix. Among other things, this allows to disable Tracer via configuration which should remove pretty much all of its potential overhead:

```
codecamp.tracer.enabled = false
```

To activate tracing for a whole service or individual methods, add the `@Traced` annotation to the respective location. This works very similar to the @Transactional annotation and one should understand how [AOP proxies in Spring](https://docs.spring.io/spring-framework/docs/current/reference/html/core.html#aop-introduction-proxies) work to understand the limitations. E.g. the annotation only works on public methods.

```
// Example: profile all public service methods except doStuffUntraced()
@Traced
@Component
public class ViaAnnotationOnService
{

  public void doStuffTraced()
  {
  }

  @Traced(ignore = true)
  public void doStuffUntraced()
  {
  }

}
```

```
// Example: only trace specific methods
@Component
public class ViaAnnotationOnServiceMethod
{

  @Traced
  public void doStuffTraced()
  {
  }

  public void doStuffUntraced()
  {
  }

}
```


## Sub-Traces

Within a @Traced method there's a `TraceContext` available that can be accessed via `Tracer.context()`. This allows you to drill down like in the example at the top (the entries that don't look like a method call) via subtraces:

```
@Traced
public void doStuff()
{
  Tracer.context().startTrace("Step 1");
  ...

  // Starting a new trace will implicitly end the previous trace on the same level.
  Tracer.context().startTrace("Step 2");
  ...
}
```

Starting a new trace will implicitly end the previous trace on the same level. If you want to drill down even further within the same method, you can
use the `ActiveTrace` returned from `startTrace(...)` (or `startSubTrace(...)`).

```
@Traced
public void doStuff()
{
  ActiveTrace traceStep1 = Tracer.context().startTrace("Step 1");
  ...

  traceStep1.startSubTrace("Step 1 a"))
  ...

  traceStep1.startSubTrace("Step 1 b"))
  ...


  Tracer.context().startTrace("Step 2");
  ...
}
```

If you need explicit control over when a trace ends, you can use the `ActiveTrace` in a try-with-resource. This can be useful if you only want to trace certain sections within a method.

```
try (ActiveTrace traceStep1 = Tracer.context().startTrace("Step 1"))
{
  try (ActiveTrace traceStep1a = traceStep1.startSubTrace("Step 1 a"))
  {
    ...
  }
}
```

Especially in this case there might be some time gaps in-between traces, which can be recorded into explicit (gap) traces.


## Gaps

For every period of time that is not directly part of a trace, a gap trace can be created to show "lost time".

```
@Traced
public void doStuff()
{
  // this might take longer than expected and could be turned into a gap trace
  ...

  // the first actual sub-trace within this method starts here
  ActiveTrace traceStep1 = Tracer.context().startTrace("Step");
  ...

}
```

This needs to be turned on explicitly and a threshold can be set to only record gaps of a certain size.

```
# application.properties

codecamp.tracer.gaps-traced = true
codecamp.tracer.gaps-threshold = 250ms
```


## Method Parameters of Traced Methods

The Spring Boot support will automatically include the parameters of @Traced methods in the label generated for the respective traces. While this provides an easy way to better comprehend what the method call was doing, some parameters may be too sensitive to be logged. In that case use the @Masked annotation on that parameter.

```
@Traced
public void doStuff(@Masked String secret)
{
  ...
}
```