package de.codecamp.tracer.impl;


import de.codecamp.tracer.ActiveTrace;
import de.codecamp.tracer.Trace;
import java.time.Instant;


public class ActiveTraceImpl
  implements
    ActiveTrace
{

  private Trace trace;

  private final TracerImpl tracer;


  public ActiveTraceImpl(Trace trace, TracerImpl tracer)
  {
    this.trace = trace;
    this.tracer = tracer;
  }


  @Override
  public ActiveTrace startSubTrace(String label, Object... labelFormatArgs)
  {
    if (trace == null)
      throw new IllegalStateException("active trace already ended");

    return TraceContextImpl.doStartTrace(tracer, trace, label, labelFormatArgs);
  }

  @Override
  public void end()
  {
    if (trace != null)
    {
      trace.end(Instant.now());
      trace = null;
    }
  }

}
