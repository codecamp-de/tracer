package de.codecamp.tracer.impl;


import de.codecamp.tracer.ActiveTrace;
import de.codecamp.tracer.Trace;
import de.codecamp.tracer.TraceContext;
import de.codecamp.tracer.TraceContextListener;
import java.time.Duration;
import java.time.Instant;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 * A trace context that may contain a number of traces.
 */
public final class TraceContextImpl
  implements
    TraceContext
{

  /**
   * the current profiler context
   */
  public static final ThreadLocal<TraceContext> CURRENT_CONTEXT = new ThreadLocal<>();


  private static final Logger LOG = Logger.getLogger(TraceContextImpl.class.getName());


  private final UUID id;

  private final String name;

  private final TraceContextImpl parentContext;

  private final FilterMode filterMode;

  private final Trace rootTrace;

  private final TracerImpl tracer;


  private Throwable exitThrowable;

  private boolean isClosed;


  private TraceContextImpl(TracerImpl tracer, TraceContextImpl parentContext, String name,
      String label, Object[] nameFormatArgs, FilterMode filterMode)
  {
    this.id = UUID.randomUUID();
    this.tracer = tracer;
    this.parentContext = parentContext;
    this.name = name;

    Trace parentTrace = null;
    if (parentContext != null)
    {
      parentTrace = parentContext.getRootTrace().getActiveTrace(true);
      if (parentTrace == null)
        throw new IllegalStateException("No active trace found in parent context.");
    }

    this.filterMode = filterMode;

    if (parentContext != null && filterMode.isExcluded())
      this.rootTrace = parentTrace;
    else
      this.rootTrace = Trace.newContextRoot(parentTrace, Instant.now(), label, nameFormatArgs);
  }


  /* package */ static TraceContextImpl newIncluded(TracerImpl tracer,
      TraceContextImpl parentContext, String contextName, String label, Object... nameFormatArgs)
  {
    return new TraceContextImpl(tracer, parentContext, contextName, label, nameFormatArgs,
        FilterMode.INCLUDED);
  }

  /* package */ static TraceContextImpl newIncludedTransitively(TracerImpl tracer,
      TraceContextImpl parentContext, String contextName, String label, Object... nameFormatArgs)
  {
    return new TraceContextImpl(tracer, parentContext, contextName, label, nameFormatArgs,
        FilterMode.INCLUDED_TRANSITIVELY);
  }

  /* package */ static TraceContextImpl newExcluded(TracerImpl tracer,
      TraceContextImpl parentContext, String contextName)
  {
    return new TraceContextImpl(tracer, parentContext, contextName, null, null,
        FilterMode.EXCLUDED);
  }

  /* package */ static TraceContextImpl newExcludedTransitively(TracerImpl tracer,
      TraceContextImpl parentContext, String contextName)
  {
    return new TraceContextImpl(tracer, parentContext, contextName, null, null,
        FilterMode.EXCLUDED_TRANSITIVELY);
  }



  @Override
  public UUID getId()
  {
    return this.id;
  }

  public String getName()
  {
    return name;
  }

  public TraceContextImpl getParentContext()
  {
    return parentContext;
  }

  public FilterMode getFilterMode()
  {
    return filterMode;
  }

  public Trace getRootTrace()
  {
    return rootTrace;
  }

  private boolean isClosed()
  {
    return isClosed;
  }

  @Override
  public boolean isRootContext()
  {
    return (parentContext == null);
  }

  @Override
  public void setExitThrowable(Throwable exitThrowable)
  {
    this.exitThrowable = exitThrowable;
  }


  @Override
  public TraceContext openContext(String fullContextName, String contextLabel,
      Object... labelFormatArgs)
  {
    return tracer.openContext(fullContextName, contextLabel, labelFormatArgs);
  }


  @Override
  public ActiveTrace startTrace(String label, Object... labelFormatArgs)
  {
    if (isClosed())
      throw new IllegalStateException("trace context was already closed");

    if (filterMode.isExcluded())
      return NoOpActiveTrace.INSTANCE;


    return doStartTrace(tracer, getRootTrace(), label, labelFormatArgs);
  }

  /* package */ static ActiveTrace doStartTrace(TracerImpl tracer, Trace parentTrace, String label,
      Object... labelFormatArgs)
  {
    Instant now = Instant.now();

    Trace subTrace;

    Trace lastSubTrace = parentTrace.getLastSubTrace();
    if (lastSubTrace != null && !lastSubTrace.hasEnded())
    {
      lastSubTrace.end(now);
      subTrace = Trace.newExplicit(parentTrace, now, label, labelFormatArgs);
    }
    else
    {
      if (tracer.isGapsIncluded())
      {
        Instant previousBoundary;
        if (lastSubTrace != null)
          previousBoundary = lastSubTrace.getEndTime();
        else
          previousBoundary = parentTrace.getStartTime();

        /* Determine if a gap trace should be inserted for any unaccounted time. */
        if (previousBoundary.isBefore(now) && Duration.between(previousBoundary, now).abs()
            .compareTo(tracer.getGapsThreshold()) >= 0)
        {
          Trace.newGap(parentTrace, previousBoundary, now);
        }
      }

      subTrace = Trace.newExplicit(parentTrace, now, label, labelFormatArgs);
    }

    return new ActiveTraceImpl(subTrace, tracer);
  }

  @Override
  public ActiveTrace getActiveTrace()
  {
    Trace activeTrace = getRootTrace().getActiveTrace(true);
    return activeTrace != null ? new ActiveTraceImpl(activeTrace, tracer)
        : NoOpActiveTrace.INSTANCE;
  }


  /**
   * Closes this trace context. This will end any currently active trace of that context.
   */
  @Override
  @SuppressWarnings("resource")
  public void close()
  {
    if (isClosed())
      throw new IllegalStateException("trace context was already closed");

    TraceContext context = CURRENT_CONTEXT.get(); // NOPMD:CloseResource
    if (context != this) // NOPMD:CompareObjectsWithEquals
    {
      throw new IllegalStateException(
          "inconsistent state; this TraceContext is expected to be the current TraceContext");
    }

    if (parentContext == null)
    {
      CURRENT_CONTEXT.remove();
    }
    else
    {
      CURRENT_CONTEXT.set(parentContext);
    }


    if (getFilterMode().isIncluded())
    {
      Instant now = Instant.now();

      // handle gap if necessary
      Trace lastSubTrace = getRootTrace().getLastSubTrace();
      if (lastSubTrace != null && lastSubTrace.hasEnded() && tracer.isGapsIncluded())
      {
        Instant previousBoundary = lastSubTrace.getEndTime();

        /* Determine if a gap trace should be inserted for any unaccounted time. */
        if (previousBoundary.isBefore(now) && Duration.between(previousBoundary, now).abs()
            .compareTo(tracer.getGapsThreshold()) >= 0)
        {
          Trace.newGap(getRootTrace(), previousBoundary, now);
        }
      }

      getRootTrace().end(now, exitThrowable);
      tracer.checkWarnThreshold(this);
    }


    isClosed = true;

    if (tracer.getListeners() != null)
    {
      for (TraceContextListener listener : tracer.getListeners())
      {
        try
        {
          listener.contextClosed(context);
        }
        catch (RuntimeException ex)
        {
          LOG.log(Level.SEVERE, "An error occurred during context-closed-notification.", ex);
        }
      }
    }

    if (tracer.getHandler() != null && isRootContext())
    {
      try
      {
        tracer.getHandler().handle(getRootTrace());
      }
      catch (RuntimeException ex)
      {
        LOG.log(Level.SEVERE, "An error occurred handling a trace.", ex);
      }
    }
  }


  @Override
  public String toString()
  {
    return "TraceContext: " + getName();
  }

}
