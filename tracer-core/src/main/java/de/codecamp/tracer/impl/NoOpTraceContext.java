package de.codecamp.tracer.impl;


import de.codecamp.tracer.ActiveTrace;
import de.codecamp.tracer.TraceContext;
import java.util.UUID;


public final class NoOpTraceContext
  implements
    TraceContext
{

  public static final TraceContext INSTANCE = new NoOpTraceContext();

  private static final UUID ID = UUID.fromString("00000000-0000-0000-0000-000000000000");


  private NoOpTraceContext()
  {
  }


  @Override
  public UUID getId()
  {
    return ID;
  }

  @Override
  public boolean isRootContext()
  {
    return true;
  }

  @Override
  public void setExitThrowable(Throwable exitThrowable)
  {
    // no-op
  }


  @Override
  public TraceContext openContext(String fullContextName, String contextLabel,
      Object... labelFormatArgs)
  {
    return INSTANCE;
  }


  @Override
  public ActiveTrace startTrace(String label, Object... labelFormatArgs)
  {
    return NoOpActiveTrace.INSTANCE;
  }

  @Override
  public ActiveTrace getActiveTrace()
  {
    return NoOpActiveTrace.INSTANCE;
  }

  @Override
  public void close()
  {
    // no-op
  }

}
