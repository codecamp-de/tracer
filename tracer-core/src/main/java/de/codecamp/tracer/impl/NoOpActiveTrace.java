package de.codecamp.tracer.impl;


import de.codecamp.tracer.ActiveTrace;


public final class NoOpActiveTrace
  implements
    ActiveTrace
{

  public static final ActiveTrace INSTANCE = new NoOpActiveTrace();


  private NoOpActiveTrace()
  {
  }


  @Override
  public ActiveTrace startSubTrace(String label, Object... labelFormatArgs)
  {
    return INSTANCE;
  }

  @Override
  public void end()
  {
    // no-op
  }

}
