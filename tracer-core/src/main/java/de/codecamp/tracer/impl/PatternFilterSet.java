package de.codecamp.tracer.impl;


import java.util.List;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class PatternFilterSet<F extends Filter>
{

  private static final String SEPARATOR = ".";


  private final List<F> filters;


  public PatternFilterSet(List<F> filters)
  {
    this.filters = filters;
  }


  public void addFilter(F filter)
  {
    this.addFilter(filter);
  }


  public Optional<F> filter(String className)
  {
    for (F filter : filters)
    {
      if (filter.getPattern().matcher(className).matches())
        return Optional.of(filter);
    }
    return Optional.empty();
  }

  public static Pattern createRegexPattern(String pattern)
  {
    StringBuilder regexPattern = new StringBuilder();

    Matcher m = Pattern.compile("\\?|\\*\\*|\\*").matcher(pattern);
    int pos = 0;
    while (m.find())
    {
      String token = pattern.substring(pos, m.start());
      if (!token.isEmpty())
        regexPattern.append(Pattern.quote(token));

      switch (m.group())
      {
        case "?" -> regexPattern.append("[^").append(Pattern.quote(SEPARATOR)).append("]");
        case "*" -> regexPattern.append("[^").append(Pattern.quote(SEPARATOR)).append("]*");
        case "**" -> regexPattern.append(".*");
        default ->
        {
          // nothing to do
        }
      }

      pos = m.end();
    }
    if (pos < pattern.length())
    {
      String token = pattern.substring(pos);
      if (!token.isEmpty())
        regexPattern.append(Pattern.quote(token));
    }

    return Pattern.compile(regexPattern.toString());
  }

}
