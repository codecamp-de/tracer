package de.codecamp.tracer.impl;


import java.util.regex.Pattern;


/**
 * A filter for includes or excludes.
 */
public class CludeFilter
  extends
    Filter
{

  public static final String TRANSITIVE_SUFFIX = "!";


  private final boolean transitive;


  public CludeFilter(String pattern, boolean transitive)
  {
    super(pattern);
    this.transitive = transitive;
  }

  public CludeFilter(Pattern pattern, boolean transitive)
  {
    super(pattern);
    this.transitive = transitive;
  }


  public boolean isTransitive()
  {
    return transitive;
  }

}
