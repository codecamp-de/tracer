package de.codecamp.tracer.impl;


import java.util.regex.Pattern;


public class Filter
{

  private final Pattern pattern;


  public Filter(String pattern)
  {
    this.pattern = PatternFilterSet.createRegexPattern(pattern);
  }

  public Filter(Pattern pattern)
  {
    this.pattern = pattern;
  }


  public Pattern getPattern()
  {
    return pattern;
  }

}
