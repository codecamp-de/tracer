package de.codecamp.tracer.impl;


import java.util.Arrays;
import java.util.Collection;
import java.util.Locale;
import java.util.Set;


/**
 * A simple wrapper for an array of parameter values that can be used as argument for the formatting
 * of trace names. The parameter values will be formatted as a comma separated list, using
 * {@link #toString()} of each value. Using this class has the advantage that the list will only be
 * formatted if the tracer actually prints or logs.
 */
public class ParamListArg
{

  /**
   * types that will be represented by their value instead of just their simple type in the profiler
   * log
   */
  private static final Set<String> PRINTABLE_TYPES = Set.of(Locale.class.getName());

  private final Object[] params;


  /**
   * Constructs a new {@link ParamListArg}.
   *
   * @param params
   *          parameter values
   */
  public ParamListArg(Object... params)
  {
    this.params = params.clone();
  }


  @Override
  public String toString()
  {
    StringBuilder sb = new StringBuilder(params.length * 16);
    boolean first = true;
    for (Object param : params)
    {
      if (first)
        first = false;
      else
        sb.append(", ");

      if (param == null || param.getClass().isPrimitive() //
          || param.getClass().getName().startsWith("java.lang") //
          || param.getClass().isEnum() //
          || PRINTABLE_TYPES.contains(param.getClass().getName()))
      {
        if (param instanceof String)
          sb.append("'").append(param).append("'");
        else
          sb.append(param);
      }
      else if (param instanceof Collection)
      {
        Collection<?> collection = (Collection<?>) param;
        sb.append(param.getClass().getSimpleName()).append("(").append(collection.size())
            .append(")");
      }
      else
      {
        boolean isArray = param.getClass().isArray();
        boolean isJavaLangElements = false;
        if (isArray)
        {
          isJavaLangElements =
              param.getClass().getComponentType().getName().startsWith("java.lang");
        }

        if (isArray && isJavaLangElements)
        {
          sb.append(Arrays.toString((Object[]) param));
        }
        else
        {
          sb.append(param.getClass().getSimpleName());
        }
      }
    }
    return sb.toString();
  }

}
