package de.codecamp.tracer.impl;


enum FilterMode
{

  INCLUDED(true, false),

  INCLUDED_TRANSITIVELY(true, true),

  EXCLUDED(false, false),

  EXCLUDED_TRANSITIVELY(false, true);


  private final boolean included;

  private final boolean transitive;


  FilterMode(boolean included, boolean transitive)
  {
    this.included = included;
    this.transitive = transitive;
  }


  public boolean isIncluded()
  {
    return included;
  }

  public boolean isExcluded()
  {
    return !included;
  }

  public boolean isTransitive()
  {
    return transitive;
  }

}
