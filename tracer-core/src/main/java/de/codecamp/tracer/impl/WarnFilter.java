package de.codecamp.tracer.impl;


import java.time.Duration;
import java.util.regex.Pattern;


public class WarnFilter
  extends
    Filter
{

  private final Duration threshold;


  public WarnFilter(Pattern pattern, Duration threshold)
  {
    super(pattern);
    this.threshold = threshold;
  }

  public WarnFilter(String pattern, Duration threshold)
  {
    super(pattern);
    this.threshold = threshold;
  }


  public Duration getThreshold()
  {
    return threshold;
  }

}
