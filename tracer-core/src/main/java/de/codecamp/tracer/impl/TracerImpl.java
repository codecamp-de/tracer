package de.codecamp.tracer.impl;


import static java.util.stream.Collectors.toList;

import de.codecamp.tracer.Trace;
import de.codecamp.tracer.TraceContext;
import de.codecamp.tracer.TraceContextListener;
import de.codecamp.tracer.TraceHandler;
import de.codecamp.tracer.Tracer;
import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Stream;
import org.apache.commons.lang3.StringUtils;


public class TracerImpl
  implements
    Tracer
{

  private static final Logger LOG = Logger.getLogger(TracerImpl.class.getName());


  private boolean gapsIncluded = true;

  private Duration gapsThreshold = Duration.ZERO;

  private PatternFilterSet<CludeFilter> includes;

  private PatternFilterSet<CludeFilter> excludes;

  private PatternFilterSet<WarnFilter> warnThresholds;

  private List<TraceContextListener> listeners;

  private TraceHandler handler;


  public TracerImpl()
  {
  }


  public boolean isGapsIncluded()
  {
    return gapsIncluded;
  }

  public void setGapsIncluded(boolean gapsIncluded)
  {
    this.gapsIncluded = gapsIncluded;
  }

  public Duration getGapsThreshold()
  {
    return gapsThreshold;
  }

  public void setGapsThreshold(Duration gapsThreshold)
  {
    this.gapsThreshold = gapsThreshold;
  }


  public void setIncludes(String[] includes)
  {
    this.includes = (includes == null) ? null : new PatternFilterSet<>(toCludeFilters(includes));
  }

  public void setExcludes(String[] excludes)
  {
    this.excludes = (excludes == null) ? null : new PatternFilterSet<>(toCludeFilters(excludes));
  }

  private List<CludeFilter> toCludeFilters(String[] includesOrExcludes)
  {
    return Stream.of(includesOrExcludes).map(String::trim).map(pattern ->
    {
      boolean transitive = false;

      if (pattern.endsWith(CludeFilter.TRANSITIVE_SUFFIX))
      {
        transitive = true;
        pattern = StringUtils.removeEnd(pattern, CludeFilter.TRANSITIVE_SUFFIX).trim();
      }

      return new CludeFilter(pattern, transitive);
    }).collect(toList());
  }

  public void setWarnThresholds(Map<String, Duration> warnThresholds)
  {
    if (warnThresholds == null)
    {
      this.warnThresholds = null;
      return;
    }

    List<WarnFilter> filters = new ArrayList<>();
    warnThresholds.forEach((pattern, threshold) ->
    {
      pattern = pattern.trim();
      filters.add(new WarnFilter(pattern, threshold));
    });
    this.warnThresholds = new PatternFilterSet<>(filters);
  }

  public TraceHandler getHandler()
  {
    return handler;
  }

  public void setHandler(TraceHandler handler)
  {
    this.handler = handler;
  }

  public List<TraceContextListener> getListeners()
  {
    return listeners;
  }

  public void setListeners(List<TraceContextListener> listeners)
  {
    this.listeners = listeners != null ? new CopyOnWriteArrayList<>(listeners) : null;
  }


  @SuppressWarnings("resource")
  @Override
  public TraceContext openContext(String fullContextName, String contextLabel,
      Object... labelFormatArgs)
  {
    if (fullContextName == null)
      throw new IllegalArgumentException("fullContextName must not be null");
    if (contextLabel == null)
      throw new IllegalArgumentException("contextLabel must not be null");


    List<TraceContextListener> listenersCopy = null;
    if (listeners != null && !listeners.isEmpty())
      listenersCopy = new ArrayList<>(listeners);


    FilterMode transitiveAncestorMode = null;

    TraceContextImpl parentContext = null; // NOPMD:CloseResource
    if (TraceContextImpl.CURRENT_CONTEXT.get() instanceof TraceContextImpl)
    {
      parentContext = (TraceContextImpl) TraceContextImpl.CURRENT_CONTEXT.get();
      // find the nearest parent context with a transitive filter mode
      TraceContextImpl ancestorContext = parentContext; // NOPMD:CloseResource
      while (ancestorContext != null)
      {
        if (ancestorContext.getFilterMode().isTransitive())
        {
          transitiveAncestorMode = ancestorContext.getFilterMode();
          break;
        }
        ancestorContext = ancestorContext.getParentContext();
      }
    }


    FilterMode filterMode;

    if (includes == null)
    {
      if (transitiveAncestorMode != null)
        filterMode = transitiveAncestorMode;
      else // if (parentContext == null)
        filterMode = FilterMode.INCLUDED_TRANSITIVELY;
    }
    else
    {
      Optional<CludeFilter> result = includes.filter(fullContextName);
      if (result.isPresent())
      {
        if (result.get().isTransitive())
          filterMode = FilterMode.INCLUDED_TRANSITIVELY;
        else if (transitiveAncestorMode == null || transitiveAncestorMode.isExcluded())
          filterMode = FilterMode.INCLUDED;
        else
          filterMode = transitiveAncestorMode;
      }
      else
      {
        if (transitiveAncestorMode != null)
          filterMode = transitiveAncestorMode;
        else // if (parentContext == null)
          filterMode = FilterMode.EXCLUDED_TRANSITIVELY;
      }
    }

    if (excludes != null)
    {
      Optional<CludeFilter> result = excludes.filter(fullContextName);
      if (result.isPresent())
      {
        if (result.get().isTransitive())
          filterMode = FilterMode.EXCLUDED_TRANSITIVELY;
        else if (filterMode == null || filterMode.isIncluded())
          filterMode = FilterMode.EXCLUDED;
      }
    }


    TraceContext newContext;
    if (parentContext == null && filterMode.isExcluded())
    {
      newContext = NoOpTraceContext.INSTANCE;
    }
    else
    {
      TraceContextImpl newContextImpl; // NOPMD:CloseResource
      switch (filterMode)
      {
        case EXCLUDED_TRANSITIVELY:
          newContextImpl =
              TraceContextImpl.newExcludedTransitively(this, parentContext, fullContextName);
          break;

        case EXCLUDED:
          newContextImpl = TraceContextImpl.newExcluded(this, parentContext, fullContextName);
          break;

        case INCLUDED:
          newContextImpl = TraceContextImpl.newIncluded(this, parentContext, fullContextName,
              contextLabel, labelFormatArgs);
          break;

        case INCLUDED_TRANSITIVELY:
        default:
          newContextImpl = TraceContextImpl.newIncludedTransitively(this, parentContext,
              fullContextName, contextLabel, labelFormatArgs);
          break;
      }

      newContext = newContextImpl;

      TraceContextImpl.CURRENT_CONTEXT.set(newContext);

      if (listenersCopy != null)
      {
        for (TraceContextListener listener : listenersCopy)
        {
          try
          {
            listener.contextOpened(newContext);
          }
          catch (RuntimeException ex)
          {
            LOG.log(Level.SEVERE, "An error occurred during context-started-notification.", ex);
          }
        }
      }
    }

    return newContext;
  }

  public void checkWarnThreshold(TraceContextImpl context)
  {
    if (warnThresholds == null)
      return;
    Optional<WarnFilter> filter = warnThresholds.filter(context.getName());
    if (!filter.isPresent())
      return;

    Trace rootTrace = context.getRootTrace();
    if (rootTrace.getDuration().compareTo(filter.get().getThreshold()) >= 0)
    {
      rootTrace.setWarn(true);
    }
  }

}
