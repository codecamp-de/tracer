package de.codecamp.tracer;


import java.util.stream.Stream;


public abstract class AbstractTraceHandler
  implements
    TraceHandler
{

  protected boolean containsWarnings(Trace trace)
  {
    return traceHierarchyStream(trace).filter(Trace::isWarn).findAny().isPresent();
  }

  protected Stream<Trace> traceHierarchyStream(Trace trace)
  {
    if (trace.getSubTraces().isEmpty())
    {
      return Stream.of(trace);
    }
    else
    {
      return Stream.concat(Stream.of(trace),
          trace.getSubTraces().stream().flatMap(this::traceHierarchyStream));
    }
  }

}
