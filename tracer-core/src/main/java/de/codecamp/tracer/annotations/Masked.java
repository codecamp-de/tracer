package de.codecamp.tracer.annotations;


import static java.lang.annotation.ElementType.PARAMETER;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;


/**
 * Indicates that the value of the annotated method parameter should not be captured. Only has an
 * effect in combination with {@link Traced}.
 */
@Target({PARAMETER})
@Retention(RUNTIME)
public @interface Masked
{

  /**
   * Returns the string that the actual value will be replaced with.
   *
   * @return the string that the actual value will be replaced with
   */
  String value() default "***";

}
