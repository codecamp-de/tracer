package de.codecamp.tracer.annotations;


import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;


/**
 * Indicates that an annotated method or methods in an annotated class should be traced. The
 * annotation can also be placed on implemented interfaces.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE, ElementType.METHOD})
@Documented
public @interface Traced
{

  /**
   * Indicates that the annotated method should not be traced. This allows to exclude individual
   * methods when the containing type is annotated.
   *
   * @return whether the annotated method should be ignored (i.e. not be traced)
   */
  boolean ignore() default false;

}
