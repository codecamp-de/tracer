package de.codecamp.tracer;


import java.util.UUID;


/**
 * The {@link TraceContext} represents a certain area of code, like a method. Contexts can be
 * nested. Each context has a root {@link Trace} that can be further divided into sub-traces.
 */
public interface TraceContext
  extends
    AutoCloseable
{

  UUID getId();

  /**
   * Trace contexts can be nested. This method returns whether this is the root context.
   *
   * @return whether this is the root context
   */
  boolean isRootContext();

  /**
   * Setting a throwable indicates that the trace context hasn't exited regularly. As a consequence,
   * durations can't necessarily be compared to other traces.
   *
   * @param exitThrowable
   *          the throwable that exited the trace
   */
  void setExitThrowable(Throwable exitThrowable);


  /**
   * Opens a new sub-context. Each context will implicitly have an associated root trace. All traces
   * started through that context will be sub-traces (i.e. children) of that root trace. If the
   * context is started within another active context (i.e. also bound to the current thread), the
   * new context's root trace will be a sub-trace of the currently active trace.
   *
   * @param fullContextName
   *          the full context name, typically the fully qualified name of the traced class suffixed
   *          with '#' and the method name, if applicable; this name is used for selection, e.g. in
   *          includes, excludes and warnings
   * @param contextLabel
   *          a label for the context; in contrast to {@code fullContextName} this is supposed to be
   *          a shorter label better suited for logging
   * @return the started trace session
   * @see Tracer#openContext(String, String)
   */
  default TraceContext openContext(String fullContextName, String contextLabel)
  {
    return openContext(fullContextName, contextLabel, (Object[]) null);
  }

  /**
   * Opens a new sub-context. Each context will implicitly have an associated root trace. All traces
   * started through that context will be sub-traces (i.e. children) of that root trace. If the
   * context is started within another active context (i.e. also bound to the current thread), the
   * new context's root trace will be a sub-trace of the currently active trace.
   *
   * @param fullContextName
   *          the full context name, typically the fully qualified name of the traced class suffixed
   *          with '#' and the method name, if applicable; this name is used for selection, e.g. in
   *          includes, excludes and warnings
   * @param contextLabel
   *          a label for the context; in contrast to {@code fullContextName} this is supposed to be
   *          a shorter label better suited for logging; may contain <code>{}</code> as placeholders
   *          that will be replaced with the given format arguments in sequence
   * @param labelFormatArgs
   *          the format arguments for the context label
   * @return the started trace context
   * @see Tracer#openContext(String, String, Object...)
   */
  TraceContext openContext(String fullContextName, String contextLabel, Object... labelFormatArgs);


  /**
   * Starts a new sub-trace as a direct child of this trace context. Ends any currently active trace
   * of this context, if there is one.
   * <p>
   * This method returns an {@link ActiveTrace} that can be used in a try-with-resource.
   *
   * @param label
   *          the label to use for this trace
   * @return an {@link ActiveTrace} that can be used in a try-with-resource
   */
  default ActiveTrace startTrace(String label)
  {
    return startTrace(label, (Object[]) null);
  }

  /**
   * Starts a new sub-trace within this trace context. Ends the currently active trace of this trace
   * context, if there is one.
   * <p>
   * v *
   *
   * @param label
   *          the label to use for this trace; may contain placeholders
   * @param labelFormatArgs
   *          the format arguments for the placeholders in the label
   * @return an {@link ActiveTrace} that can be used in a try-with-resource
   */
  ActiveTrace startTrace(String label, Object... labelFormatArgs);

  /**
   * Returns the deepest currently active trace of the current context or a no-op dummy if none is
   * available.
   *
   * @return the deepest currently active trace of the current context or a no-op dummy if none is
   *         available.
   */
  ActiveTrace getActiveTrace();

  /**
   * Closes this trace context. Closing a trace context is the responsibility of the one opening it.
   * <p>
   * Overridden from {@link AutoCloseable#close()} to remove thrown {@link Exception}.
   */
  @Override
  void close();

}
