package de.codecamp.tracer;


import static java.util.Objects.requireNonNull;

import de.codecamp.tracer.impl.TraceContextImpl;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import org.slf4j.helpers.MessageFormatter;


/**
 * A {@link Trace} represents a time interval an application has spent within a certain piece of
 * code. A trace can be part of a hierarchy, i.e. i can have a parent trace and it can have
 * sub-traces.
 */
public final class Trace
{

  private final boolean isGap;

  private String label;

  private Object[] labelFormatArgs;


  private final Instant startTime;

  private Instant endTime;


  private final Trace parentTrace;

  private List<Trace> subTraces;

  private List<Trace> subTracesReadOnly;

  private final boolean isContextRoot;


  private Throwable exitThrowable;

  private boolean warn;

  private Map<String, Object> dataMap;


  /**
   * Constructs a new {@link Trace}.
   */
  private Trace(Trace parentTrace, boolean isContextRoot, boolean isGap, Instant startTime,
      Instant endTime, String label, Object[] labelFormatArgs)
  {
    this.isGap = isGap;
    this.label = label;
    this.labelFormatArgs = labelFormatArgs;

    this.parentTrace = parentTrace;
    this.isContextRoot = isContextRoot;

    this.startTime = startTime;
    this.endTime = endTime;

    if (parentTrace != null)
    {
      parentTrace.addSubTrace(this);
    }
  }


  public static Trace newGap(Trace parentTrace, Instant startTime, Instant endTime)
  {
    return new Trace(parentTrace, false, true, startTime, endTime, "?", null);
  }

  public static Trace newExplicit(Trace parentTrace, Instant startTime, String label,
      Object[] labelFormatArgs)
  {
    return new Trace(parentTrace, false, false, startTime, null, label, labelFormatArgs);
  }

  public static Trace newContextRoot(Trace parentTrace, Instant startTime, String label,
      Object[] labelFormatArgs)
  {
    return new Trace(parentTrace, true, false, startTime, null, label, labelFormatArgs);
  }


  /**
   * Returns the label of this trace.
   *
   * @return the label of this trace
   */
  public String getLabel()
  {
    if (labelFormatArgs != null && labelFormatArgs.length > 0)
    {
      label = MessageFormatter.arrayFormat(label, labelFormatArgs).getMessage();
      labelFormatArgs = null;
    }

    return label;
  }

  /**
   * Returns the start time of this trace.
   *
   * @return the start time of this trace
   */
  public Instant getStartTime()
  {
    return startTime;
  }

  /**
   * Returns the end time of this trace.
   *
   * @return the end time of this trace
   * @throws IllegalStateException
   *           if the trace has not yet finished
   */
  public Instant getEndTime()
  {
    if (!hasEnded())
      throw new IllegalStateException("the trace has not yet finished");

    return endTime;
  }

  /**
   * Returns whether the trace has already ended.
   *
   * @return whether the trace has already ended
   */
  public boolean hasEnded()
  {
    return endTime != null;
  }

  /**
   * Sets the trace as ended and records the {@link #getEndTime() end time}.
   *
   * @param endTime
   *          the end time for the trace
   */
  public void end(Instant endTime)
  {
    end(endTime, null);
  }

  /**
   * Sets this trace and all still active sub-traces as ended and records the {@link #getEndTime()
   * end time}.
   *
   * @param endTime
   *          the end time for the trace
   * @param exitThrowable
   *          the throwable that exited the trace
   */
  public void end(Instant endTime, Throwable exitThrowable)
  {
    requireNonNull(endTime, "endTime must not be null");

    if (hasEnded())
      throw new IllegalStateException("trace has already ended");

    this.exitThrowable = exitThrowable;

    Trace t = this;
    while (true)
    {
      if (t.hasEnded())
        break;

      t.endTime = endTime;

      if (subTraces == null || subTraces.isEmpty())
        break;

      t = subTraces.get(subTraces.size() - 1);
    }
  }

  /**
   * Returns the duration of this trace.
   *
   * @return the duration of this trace
   * @throws IllegalStateException
   *           if the trace has not yet finished
   */
  public Duration getDuration()
  {
    /*
     * Instant has nanosecond precision. Using Duration.between(..) would give the exact duration,
     * but only using milliseconds during formatting leads to the traces not quite adding up to the
     * total duration, most likely due to truncating / rounding down.
     *
     * Using toEpochMilli() and calculating the millisecond duration oneself leads to the individual
     * durations all adding up nicely. Though, it's unclear why exactly...
     */
    return Duration.ofMillis(getEndTime().toEpochMilli() - getStartTime().toEpochMilli());
  }

  /**
   * Returns the parent trace or null.
   *
   * @return the parent trace or null
   */
  public Trace getParentTrace()
  {
    return parentTrace;
  }

  /**
   * Returns whether this trace is the root of a {@link TraceContextImpl}.
   *
   * @return whether this trace is the root of a {@link TraceContextImpl}
   */
  public boolean isContextRoot()
  {
    return isContextRoot;
  }

  /**
   * Returns whether this trace represents a gap. Gaps are periods of time between explicit traces.
   * This can help locate costly segments of code that aren't explicitly traced (yet).
   *
   * @return whether this trace represents a gap
   */
  public boolean isGap()
  {
    return isGap;
  }

  /**
   * Returns the list of sub-traces.
   *
   * @return the list of sub-traces
   */
  public List<Trace> getSubTraces()
  {
    if (subTraces == null)
      return Collections.emptyList();

    if (subTracesReadOnly == null)
      subTracesReadOnly = Collections.unmodifiableList(subTraces);

    return subTracesReadOnly;
  }

  public Trace getLastSubTrace()
  {
    List<Trace> st = getSubTraces();
    if (st.isEmpty())
    {
      return null;
    }
    else
    {
      return st.get(st.size() - 1);
    }
  }

  public Trace getActiveTrace(boolean includeRoot)
  {
    Trace activeTrace = null;

    Trace t = this;
    while (true)
    {
      if (t.hasEnded())
        break;
      if (t != this && t.isContextRoot()) // NOPMD:CompareObjectsWithEquals
        break;

      if (includeRoot || t != this) // NOPMD:CompareObjectsWithEquals
        activeTrace = t;

      if (t.subTraces == null || t.subTraces.isEmpty())
        break;

      t = t.subTraces.get(t.subTraces.size() - 1);
    }

    return activeTrace;
  }

  private void addSubTrace(Trace trace)
  {
    if (subTraces == null)
      subTraces = new ArrayList<>();

    subTraces.add(trace);
  }


  /**
   * Returns the throwable that exited the trace.
   *
   * @return the throwable that exited the trace
   */
  public Optional<Throwable> getExitThrowable()
  {
    return Optional.ofNullable(exitThrowable);
  }


  public boolean isWarn()
  {
    return warn;
  }

  public void setWarn(boolean warn)
  {
    this.warn = warn;
  }


  public <T> T getData(Class<T> key)
  {
    return key.cast(dataMap.get(key.getClass().getName()));
  }

  public <T> void setData(Class<T> key, T data)
  {
    dataMap.put(key.getClass().getName(), data);
  }

  public Object getData(String key)
  {
    return dataMap.get(key);
  }

  public <T> T getData(String key, Class<T> type)
  {
    return type.cast(dataMap.get(key));
  }

  public void setData(String key, Object data)
  {
    dataMap.put(key, data);
  }

  public Map<String, Object> getData()
  {
    return dataMap;
  }

  public void setData(Map<String, Object> data)
  {
    dataMap.clear();
    dataMap.putAll(data);
  }


  @Override
  public String toString()
  {
    return "Trace(" + getParentTrace() + " -> " + getLabel() + ")";
  }

}
