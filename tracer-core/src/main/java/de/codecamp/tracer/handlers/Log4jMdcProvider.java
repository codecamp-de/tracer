package de.codecamp.tracer.handlers;


import de.codecamp.tracer.TraceContext;
import de.codecamp.tracer.TraceContextListener;
import org.apache.logging.log4j.ThreadContext;


public class Log4jMdcProvider
  implements
    TraceContextListener
{

  private static final String MDCKEY_TRACE_CONTEXT_ID = "TraceContextId";


  @Override
  public void contextOpened(TraceContext context)
  {
    if (context.isRootContext())
      ThreadContext.put(MDCKEY_TRACE_CONTEXT_ID, context.getId().toString());
  }

  @Override
  public void contextClosed(TraceContext context)
  {
    if (context.isRootContext())
      ThreadContext.remove(MDCKEY_TRACE_CONTEXT_ID);
  }

}
