package de.codecamp.tracer.handlers;


import de.codecamp.tracer.AbstractTraceHandler;
import de.codecamp.tracer.Trace;
import de.codecamp.tracer.TraceFormatter;
import de.codecamp.tracer.TraceHandler;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;


/**
 * A {@link TraceHandler} logging with Apache Commons Logging. Also suitable when using the Spring
 * Commons Logging Bridge (spring-jcl).
 */
public class JclLoggingTraceHandler
  extends
    AbstractTraceHandler
{

  private final Log log;

  private TraceFormatter formatter;


  public JclLoggingTraceHandler()
  {
    this("de.codecamp.tracer");
  }

  public JclLoggingTraceHandler(String loggerName)
  {
    this(LogFactory.getLog(loggerName));
  }

  public JclLoggingTraceHandler(Log log)
  {
    this.log = log;
  }


  /**
   * Sets the formatter used when logging. Only relevant on the root session.
   *
   * @param formatter
   *          the formatter
   */
  public void setFormatter(TraceFormatter formatter)
  {
    this.formatter = formatter;
  }


  @Override
  public void handle(Trace trace)
  {
    if (containsWarnings(trace))
      log.warn(formatter.format(trace));
    else
      log.trace(formatter.format(trace));
  }

}
