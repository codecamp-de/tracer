package de.codecamp.tracer.handlers;


import de.codecamp.tracer.TraceContext;
import de.codecamp.tracer.TraceContextListener;
import org.slf4j.MDC;


public class Slf4jMdcProvider
  implements
    TraceContextListener
{

  private static final String MDCKEY_TRACE_CONTEXT_ID = "TraceContextId";


  @Override
  public void contextOpened(TraceContext context)
  {
    if (context.isRootContext())
      MDC.put(MDCKEY_TRACE_CONTEXT_ID, context.getId().toString());
  }

  @Override
  public void contextClosed(TraceContext context)
  {
    if (context.isRootContext())
      MDC.remove(MDCKEY_TRACE_CONTEXT_ID);
  }

}
