package de.codecamp.tracer.handlers;


import de.codecamp.tracer.AbstractTraceHandler;
import de.codecamp.tracer.Trace;
import de.codecamp.tracer.TraceFormatter;
import de.codecamp.tracer.TraceHandler;
import java.util.logging.Logger;


/**
 * A {@link TraceHandler} logging with {@code java.util.logging}.
 */
public class JulLoggingTraceHandler
  extends
    AbstractTraceHandler
{

  private final Logger logger;

  private TraceFormatter formatter;


  public JulLoggingTraceHandler()
  {
    this("de.codecamp.tracer");
  }

  public JulLoggingTraceHandler(String loggerName)
  {
    logger = Logger.getLogger(loggerName);
  }


  /**
   * Sets the formatter used when logging. Only relevant on the root session.
   *
   * @param formatter
   *          the formatter
   */
  public void setFormatter(TraceFormatter formatter)
  {
    this.formatter = formatter;
  }


  @Override
  public void handle(Trace trace)
  {
    if (containsWarnings(trace))
      logger.warning(formatter.format(trace));
    else
      logger.fine(formatter.format(trace));
  }

}
