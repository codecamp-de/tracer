package de.codecamp.tracer.handlers;


import de.codecamp.tracer.AbstractTraceHandler;
import de.codecamp.tracer.Trace;
import de.codecamp.tracer.TraceFormatter;
import de.codecamp.tracer.TraceHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;


/**
 * A {@link TraceHandler} logging with SLF4J.
 */
public class Slf4jLoggingTraceHandler
  extends
    AbstractTraceHandler
{

  /**
   * the name of the {@link Marker marker} used when logging the profiler session
   */
  public static final String MARKER_NAME = "tracer";


  private static final Marker MARKER = MarkerFactory.getMarker(MARKER_NAME);


  private final Logger logger;

  private TraceFormatter formatter;


  public Slf4jLoggingTraceHandler()
  {
    this("de.codecamp.tracer");
  }

  public Slf4jLoggingTraceHandler(String loggerName)
  {
    logger = LoggerFactory.getLogger(loggerName);
  }


  /**
   * Sets the formatter used when logging. Only relevant on the root session.
   *
   * @param formatter
   *          the formatter
   */
  public void setFormatter(TraceFormatter formatter)
  {
    this.formatter = formatter;
  }


  @Override
  public void handle(Trace trace)
  {
    if (containsWarnings(trace))
      logger.warn(MARKER, formatter.format(trace));
    else
      logger.trace(MARKER, formatter.format(trace));
  }

}
