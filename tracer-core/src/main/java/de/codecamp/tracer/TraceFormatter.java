package de.codecamp.tracer;


/**
 * Responsible for bringing the trace hierarchy into an suitable printable or loggable format.
 */
public interface TraceFormatter
{

  /**
   * Formats the given trace including all subtraces.
   *
   * @param trace
   *          the trace to be formatted
   * @return the formatted string
   */
  String format(Trace trace);

}
