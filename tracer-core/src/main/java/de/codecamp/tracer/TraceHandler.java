package de.codecamp.tracer;


/**
 * The {@link TraceHandler} is responsible for processing a fully completed trace, i.e. the handler
 * is only called for the root trace of the root context.
 */
public interface TraceHandler
{

  /**
   * Handles the given fully completed trace.
   *
   * @param trace
   *          the fully completed trace (a root trace of a root context)
   */
  void handle(Trace trace);

}
