package de.codecamp.tracer;


import de.codecamp.tracer.impl.NoOpTraceContext;
import de.codecamp.tracer.impl.ParamListArg;
import de.codecamp.tracer.impl.TraceContextImpl;


/**
 * A simple tool for hierarchical measurement of durations in your code.
 */
public interface Tracer
{

  /**
   * Opens a new tracer context. Each context will implicitly have an associated root trace. All
   * traces started through that context will be sub-traces (i.e. children) of that root trace. If
   * the context is started within another active context (i.e. also bound to the current thread),
   * the new context's root trace will be a sub-trace of the currently active trace.
   *
   * @param fullContextName
   *          the full context name, typically the fully qualified name of the traced class suffixed
   *          with '#' and the method name, if applicable; this name is used for selection, e.g. in
   *          includes, excludes and warnings
   * @param contextLabel
   *          a label for the context; in contrast to {@code fullContextName} this is supposed to be
   *          a shorter label better suited for logging
   * @return the started trace session
   */
  default TraceContext openContext(String fullContextName, String contextLabel)
  {
    return openContext(fullContextName, contextLabel, (Object[]) null);
  }

  /**
   * Opens a new trace context. Each context will implicitly have an associated root trace. All
   * traces started through that context will be sub-traces (i.e. children) of that root trace. If
   * the context is started within another active context (i.e. also bound to the current thread),
   * the new context's root trace will be a sub-trace of the currently active trace.
   *
   * @param fullContextName
   *          the full context name, typically the fully qualified name of the traced class suffixed
   *          with '#' and the method name, if applicable; this name is used for selection, e.g. in
   *          includes, excludes and warnings
   * @param contextLabel
   *          a label for the context; in contrast to {@code fullContextName} this is supposed to be
   *          a shorter label better suited for logging; may contain <code>{}</code> as placeholders
   *          that will be replaced with the given format arguments in sequence
   * @param labelFormatArgs
   *          the format arguments for the context label
   * @return the started trace context
   */
  TraceContext openContext(String fullContextName, String contextLabel, Object... labelFormatArgs);

  /**
   * Opens a new trace context. Each context will implicitly have an associated root trace. All
   * traces started through that context will be sub-traces (i.e. children) of that root trace. If
   * the context is started within another active context (i.e. also bound to the current thread),
   * the new context's root trace will be a sub-trace of the currently active trace.
   *
   * @param tracedClass
   *          the traced class
   * @param methodName
   *          the traced method
   * @param paramValues
   *          the method parameter values
   * @return the started trace context
   */
  default TraceContext openContext(Class<?> tracedClass, String methodName, Object... paramValues)
  {
    String fullContextName = String.format("%s#%s", tracedClass.getCanonicalName(), methodName);
    return openContext(fullContextName, "{}#{}({})", tracedClass.getSimpleName(), methodName,
        new ParamListArg(paramValues));
  }


  /**
   * Returns whether a tracer context is currently open for the current thread.
   *
   * @return whether a tracer context is currently open for the current thread
   */
  static boolean hasContext()
  {
    return TraceContextImpl.CURRENT_CONTEXT.get() != null;
  }

  /**
   * Returns the currently open tracer context or a no-op dummy if none is available.
   *
   * @return the currently open tracer context or a no-op dummy; never null
   */
  static TraceContext context()
  {
    TraceContext traceContext = TraceContextImpl.CURRENT_CONTEXT.get();
    if (traceContext == null)
      return NoOpTraceContext.INSTANCE;

    return traceContext;
  }

  /**
   * Returns the deepest currently active trace of the current context or a no-op dummy if none is
   * available.
   *
   * @return the deepest currently active trace of the current context or a no-op dummy if none is
   *         available.
   */
  static ActiveTrace activeTrace()
  {
    return context().getActiveTrace();
  }

}
