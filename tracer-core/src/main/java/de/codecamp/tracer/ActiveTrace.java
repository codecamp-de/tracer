package de.codecamp.tracer;


/**
 * An {@link ActiveTrace} is a short-lived delegate that represents an active trace. It's generally
 * recommended to use it in a try-with-resource statement to safely control its start and end.
 */
public interface ActiveTrace
  extends
    AutoCloseable
{

  /**
   * Starts and returns a new sub-trace which will be the new active trace. It's generally
   * recommended to call this method in a try-with-resource.
   *
   * @param label
   *          the label to use for this trace; may contain placeholders
   * @param labelFormatArgs
   *          the format arguments for the placeholders in the label
   * @return the new active sub-trace
   */
  ActiveTrace startSubTrace(String label, Object... labelFormatArgs);

  /**
   * Ends this trace and all sub-traces.
   */
  void end();

  /**
   * Simply calls {@link #end()}.
   */
  @Override
  default void close()
  {
    end();
  }

}
