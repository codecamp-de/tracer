package de.codecamp.tracer.formatters;


import de.codecamp.tracer.Trace;
import de.codecamp.tracer.TraceFormatter;
import java.text.DecimalFormat;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang3.ClassUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Triple;


/**
 * The default profile formatter.
 */
public class DefaultTraceFormatter
  implements
    TraceFormatter
{

  private static final DateTimeFormatter DATETIME_FORMAT =
      DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS");

  private static final DateTimeFormatter TIME_FORMAT = DateTimeFormatter.ofPattern("HH:mm:ss");

  private static final DateTimeFormatter TIME_FORMAT_WITH_MILLIS =
      DateTimeFormatter.ofPattern("HH:mm:ss.SSS");

  private static final Map<TreeStyle, String[]> TREE_STYLE_SYMBOLS = new HashMap<>();
  static
  {
    TREE_STYLE_SYMBOLS.put(TreeStyle.SPACES, new String[] {"  ", "  ", "  ", "  "});
    TREE_STYLE_SYMBOLS.put(TreeStyle.ARROWS, new String[] {"> ", "> ", "> ", "> "});
    TREE_STYLE_SYMBOLS.put(TreeStyle.DOTS, new String[] {"• ", "• ", "• ", "• "});
    TREE_STYLE_SYMBOLS.put(TreeStyle.LINES, new String[] {"├ ", "└ ", "│ ", "  "});
  }


  private int lineWidth = 100;

  private boolean prefixNewLine = false;

  private TreeStyle treeStyle = TreeStyle.SPACES;


  /**
   * Constructs a new {@link DefaultTraceFormatter}.
   */
  public DefaultTraceFormatter()
  {
  }


  public void setLineWidth(int lineWidth)
  {
    this.lineWidth = lineWidth;
  }

  public void setPrefixNewLine(boolean prefixNewLine)
  {
    this.prefixNewLine = prefixNewLine;
  }

  public void setTreeStyle(TreeStyle treeStyle)
  {
    this.treeStyle = treeStyle;
  }


  @Override
  public String format(Trace trace)
  {
    boolean durationInMillis = trace.getDuration().toMillis() <= 9999;

    List<Triple<String, String, Integer>> lines = new ArrayList<>();
    formatTrace(lines, trace, true, 0, "", durationInMillis);

    int labelColumnWidth = 0;
    int durationColumnWidth = 0;
    for (Triple<String, String, Integer> line : lines)
    {
      labelColumnWidth = Math.max(line.getLeft().length(), labelColumnWidth);
      if (line.getMiddle() != null)
        durationColumnWidth = Math.max(line.getMiddle().length(), durationColumnWidth);
    }

    int staticAdditionsWidth = 5;

    int minWidth = labelColumnWidth + durationColumnWidth + staticAdditionsWidth;
    int actualLineWidth = Math.max(minWidth, lineWidth);


    StringBuilder sb = new StringBuilder();

    if (prefixNewLine)
      sb.append("\n");

    String dateString = DATETIME_FORMAT.format(trace.getStartTime().atZone(ZoneId.systemDefault()));
    sb.append(StringUtils.rightPad(dateString + " ", actualLineWidth, "="));
    sb.append("\n");


    int labelTargetWidth = actualLineWidth - durationColumnWidth - staticAdditionsWidth;

    for (Triple<String, String, Integer> line : lines)
    {
      String label = line.getLeft();
      String duration = line.getMiddle();

      int labelPadWidth = labelTargetWidth - label.length();
      if (duration != null)
      {
        sb.append(label);
        sb.append(" .");
        sb.append(StringUtils.repeat(".", labelPadWidth));
        sb.append(" [");
        sb.append(StringUtils.leftPad(duration, durationColumnWidth));
        sb.append("]");
      }
      else
      {
        // currently this is always an exception
        sb.append(StringUtils.leftPad(label, actualLineWidth));
      }
      sb.append("\n");
    }

    return sb.toString();
  }

  private void formatTrace(List<Triple<String, String, Integer>> lines, Trace trace,
      boolean isLastChild, int depth, String labelPrefix, boolean durationInMillis)
  {
    StringBuilder labelBuilder = new StringBuilder();
    labelBuilder.append(labelPrefix);

    if (depth > 0)
    {
      String[] treeStyles = TREE_STYLE_SYMBOLS.get(treeStyle);
      labelBuilder.append(isLastChild ? treeStyles[1] : treeStyles[0]);
    }

    labelBuilder.append(trace.getLabel());
    if (trace.isContextRoot())
    {
      labelBuilder.append(" *");
    }

    String duration;
    if (durationInMillis)
    {
      duration = Long.toString(trace.getDuration().toMillis()) + " ms";
    }
    else
    {
      DecimalFormat df = new DecimalFormat();
      df.setMinimumFractionDigits(3);
      df.setMaximumFractionDigits(3);
      String durationString = df.format(trace.getDuration().toMillis() / 1000f);
      duration = durationString + " s";
    }

    if (trace.isWarn())
      duration = "! " + duration;

    duration += " @ " + (durationInMillis ? TIME_FORMAT_WITH_MILLIS : TIME_FORMAT)
        .format(trace.getStartTime().atZone(ZoneId.systemDefault()));

    lines.add(Triple.of(labelBuilder.toString(), duration, depth));
    trace.getExitThrowable().ifPresent(t ->
    {
      String className = ClassUtils.getAbbreviatedName(t.getClass(), Math.max(lineWidth - 1, 1));
      lines.add(Triple.of("!" + className, null, depth));
    });

    List<Trace> subTraces = trace.getSubTraces();
    for (int i = 0; i < subTraces.size(); i++)
    {
      Trace subTrace = subTraces.get(i);

      String childLabelPrefix;
      if (depth > 0)
      {
        String[] symbols = TREE_STYLE_SYMBOLS.get(treeStyle);
        childLabelPrefix = labelPrefix + (isLastChild ? symbols[3] : symbols[2]);
      }
      else
        childLabelPrefix = "";

      formatTrace(lines, subTrace, i == subTraces.size() - 1, depth + 1, childLabelPrefix,
          durationInMillis);
    }
  }


  public enum TreeStyle
  {

    SPACES,
    LINES,
    ARROWS,
    DOTS

  }

}
