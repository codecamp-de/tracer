package de.codecamp.tracer;


/**
 * A listener that gets notified when trace contexts are started and closed. Implementations should
 * be very quick.
 */
public interface TraceContextListener
{

  /**
   * Called when a trace context was opened.
   *
   * @param context
   *          the trace context
   */
  void contextOpened(TraceContext context);

  /**
   * Called when a trace context was closed.
   *
   * @param context
   *          the trace context
   */
  void contextClosed(TraceContext context);

}
