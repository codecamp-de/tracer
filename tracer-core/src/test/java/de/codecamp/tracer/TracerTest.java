package de.codecamp.tracer;


import static org.assertj.core.api.Assertions.assertThat;

import de.codecamp.tracer.impl.NoOpTraceContext;
import de.codecamp.tracer.impl.TracerImpl;
import org.junit.jupiter.api.Test;


class TracerTest
{

  @Test
  void staticThreadLocalAccessToContext()
  {
    Tracer tracer = new TracerImpl();

    assertThat(Tracer.hasContext()).isFalse();
    assertThat(Tracer.context()).isInstanceOf(NoOpTraceContext.class);

    try (TraceContext context = tracer.openContext(TracerTest.class.getCanonicalName(), "test"))
    {
      assertThat(Tracer.hasContext()).isTrue();
      assertThat(Tracer.context()).isSameAs(context);
    }

    assertThat(Tracer.hasContext()).isFalse();
  }

}
