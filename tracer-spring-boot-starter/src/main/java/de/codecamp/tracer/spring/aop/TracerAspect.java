package de.codecamp.tracer.spring.aop;


import de.codecamp.tracer.TraceContext;
import de.codecamp.tracer.Tracer;
import de.codecamp.tracer.annotations.Masked;
import de.codecamp.tracer.annotations.Traced;
import de.codecamp.tracer.impl.ParamListArg;
import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.util.ClassUtils;


@Aspect
public class TracerAspect
{

  private Tracer tracer;


  public void setTracer(Tracer tracer)
  {
    this.tracer = tracer;
  }


  /*
   * include all public methods on types annotated with @Traced and all methods directly annotated
   * with @Traced
   */
  @Pointcut("@within(de.codecamp.tracer.annotations.Traced) || (@annotation(de.codecamp.tracer.annotations.Traced) && execution(public * *(..)))")
  private void tracedMethods()
  {
    // pointcut
  }


  @Around("tracedMethods()")
  public Object profileAdvice(ProceedingJoinPoint pjp)
    throws Throwable
  {
    MethodSignature methodSig = (MethodSignature) pjp.getSignature();
    Traced annotation = findAnnotation(methodSig.getMethod(), pjp.getTarget().getClass());

    Class<? extends Object> tracedClass = pjp.getTarget().getClass();

    if (annotation != null && !annotation.ignore())
    {
      String methodName = methodSig.getName();
      Object[] paramValues = pjp.getArgs().clone();

      Annotation[][] allAnnotations = methodSig.getMethod().getParameterAnnotations();
      for (int i = 0; i < allAnnotations.length; i++)
      {
        for (Annotation a : allAnnotations[i])
        {
          if (a instanceof Masked)
          {
            paramValues[i] = ((Masked) a).value();
            break;
          }
        }
      }

      String fullContextName = String.format("%s#%s", tracedClass.getCanonicalName(), methodName);

      try (TraceContext tc = tracer.openContext(fullContextName, "{}#{}({})",
          tracedClass.getSimpleName(), methodName, new ParamListArg(paramValues)))
      {
        try
        {
          return pjp.proceed();
        }
        catch (Throwable t) // NOPMD:AvoidCatchingThrowable
        {
          tc.setExitThrowable(t);
          throw t;
        }
      }
    }
    else
    {
      return pjp.proceed();
    }
  }

  /**
   * Detects and returns the {@link Traced} annotation on the given method and class if present. The
   * fallback is like this:
   * <ol>
   * <li>check method of the implementation</li>
   * <li>check class of the implementation</li>
   * <li>check method of interface</li>
   * <li>check class of interface</li>
   * </ol>
   */
  private static Traced findAnnotation(Method method, Class<?> targetClass)
  {
    /*
     * This is similar to
     * org.springframework.data.repository.core.support.TransactionalRepositoryProxyPostProcessor
     */

    // ignore CGLIB subclasses, get the actual user class
    Class<?> userClass = ClassUtils.getUserClass(targetClass);
    // The method may be on an interface, but we need attributes from the target class.
    // If the target class is null, the method will be unchanged.
    Method specificMethod = ClassUtils.getMostSpecificMethod(method, userClass);

    Traced annotation = AnnotationUtils.getAnnotation(specificMethod, Traced.class);
    if (annotation == null)
    {
      annotation = AnnotationUtils.getAnnotation(specificMethod.getDeclaringClass(), Traced.class);
    }
    if (annotation == null && !specificMethod.equals(method))
    {
      annotation = AnnotationUtils.getAnnotation(method, Traced.class);
      if (annotation == null)
      {
        annotation = AnnotationUtils.getAnnotation(method.getDeclaringClass(), Traced.class);
      }
    }
    return annotation;
  }

}
