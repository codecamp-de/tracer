package de.codecamp.tracer.spring.autoconfigure;


import de.codecamp.tracer.TraceContextListener;
import de.codecamp.tracer.TraceFormatter;
import de.codecamp.tracer.TraceHandler;
import de.codecamp.tracer.Tracer;
import de.codecamp.tracer.formatters.DefaultTraceFormatter;
import de.codecamp.tracer.handlers.JclLoggingTraceHandler;
import de.codecamp.tracer.handlers.Log4jMdcProvider;
import de.codecamp.tracer.handlers.Slf4jMdcProvider;
import de.codecamp.tracer.impl.TracerImpl;
import de.codecamp.tracer.spring.aop.TracerAspect;
import java.time.Duration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.boot.autoconfigure.AutoConfiguration;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.convert.DurationStyle;
import org.springframework.context.annotation.Bean;


@AutoConfiguration
@ConditionalOnProperty(prefix = "codecamp.tracer", name = "enabled", matchIfMissing = true)
@EnableConfigurationProperties(TracerPropertiers.class)
public class TracerAutoConfiguration
{

  @Bean
  TracerAspect profilerAspect(Tracer tracer)
  {
    TracerAspect bean = new TracerAspect();

    bean.setTracer(tracer);

    return bean;
  }

  @Bean
  @ConditionalOnMissingBean
  Tracer tracer(TracerPropertiers properties, TraceHandler traceHandler,
      List<TraceContextListener> listeners)
  {
    TracerImpl bean = new TracerImpl();

    bean.setGapsIncluded(properties.isGapsTraced());
    bean.setGapsThreshold(properties.getGapsThreshold());
    bean.setIncludes(properties.getIncludes());
    bean.setExcludes(properties.getExcludes());

    String[] warnThresholds = properties.getWarnThresholds();
    if (warnThresholds != null)
    {
      Map<String, Duration> warnThresholdsConverted = new HashMap<>();
      for (String warnThreshold : warnThresholds)
      {
        String[] tokens = warnThreshold.split("[/:]", 2);
        String patternString = tokens[0].trim();
        String durationString = tokens[1].trim();
        warnThresholdsConverted.put(patternString, DurationStyle.detectAndParse(durationString));
      }
      bean.setWarnThresholds(warnThresholdsConverted);
    }

    bean.setHandler(traceHandler);
    bean.setListeners(listeners);

    return bean;
  }

  @Bean
  @ConditionalOnMissingBean
  public TraceHandler traceHandler(TracerPropertiers properties, TraceFormatter traceFormatter)
  {
    JclLoggingTraceHandler bean = new JclLoggingTraceHandler(properties.getLoggerName());

    bean.setFormatter(traceFormatter);

    return bean;
  }

  @Bean
  @ConditionalOnMissingBean
  TraceFormatter traceFormatter(TracerPropertiers properties)
  {
    DefaultTraceFormatter bean = new DefaultTraceFormatter();

    bean.setLineWidth(properties.getFormatter().getLineWidth());
    bean.setPrefixNewLine(properties.getFormatter().getPrefixNewLine());
    bean.setTreeStyle(properties.getFormatter().getTreeStyle());

    return bean;
  }

  @Bean
  @ConditionalOnClass(name = "org.slf4j.MDC")
  TraceContextListener slf4jMdcProvider()
  {
    return new Slf4jMdcProvider();
  }

  @Bean
  @ConditionalOnClass(name = "org.apache.logging.log4j.ThreadContext")
  TraceContextListener log4jMdcProvider()
  {
    return new Log4jMdcProvider();
  }

}
