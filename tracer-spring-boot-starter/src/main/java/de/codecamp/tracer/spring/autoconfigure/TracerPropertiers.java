package de.codecamp.tracer.spring.autoconfigure;


import de.codecamp.tracer.formatters.DefaultTraceFormatter.TreeStyle;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.PositiveOrZero;
import java.time.Duration;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.NestedConfigurationProperty;
import org.springframework.validation.annotation.Validated;


@ConfigurationProperties(prefix = "codecamp.tracer")
@Validated
public class TracerPropertiers
{

  /**
   * Whether to enable tracing.
   */
  private boolean enabled = true;

  /**
   * The logger name used to log the traces.
   */
  @NotBlank
  private String loggerName = "de.codecamp.tracer";

  /**
   * Whether gaps between two explicitly traced segments should get their own trace. This can help
   * pinpointing "lost" time. See <b>gaps-threshold</b> to only include gaps above a certain size.
   */
  private boolean gapsTraced = false;

  /**
   * The threshold to create a gap trace. Durations below this threshold will NOT result in a gap
   * trace.
   */
  @NotNull
  private Duration gapsThreshold = Duration.ZERO;

  /**
   * A list of context name patterns to include in tracing. Append an <b>!</b> to a pattern to also
   * include all sub-contexts. When empty, everything is included. Excludes always take precedence
   * over includes. The supported wildcards in the patterns are:
   * <ul>
   * <li><b>?</b> matches any single character, except <b>.</b></li>
   * <li><b>&ast;</b> matches any sequence of characters (including empty string), except
   * <b>.</b></li>
   * <li><b>&ast;&ast;</b> matches any sequence of characters (including empty string), including
   * <b>.</b></li>
   * </ul>
   */
  private String[] includes;

  /**
   * A list of context name patterns to exclude from being traced. Append an <b>!</b> to a pattern
   * to also exclude all sub-contexts. Excludes always take precedence over includes. The supported
   * wildcards in the patterns are:
   * <ul>
   * <li><b>?</b> matches any single character, except <b>.</b></li>
   * <li><b>&ast;</b> matches any sequence of characters (including empty string), except
   * <b>.</b></li>
   * <li><b>&ast;&ast;</b> matches any sequence of characters (including empty string), including
   * <b>.</b></li>
   * </ul>
   */
  private String[] excludes;

  /**
   * A list of context name patterns plus duration (separated by <b>:</b> or <b>/</b>) to define
   * thresholds above which traces will be logged as a warning. The supported wildcards in the
   * patterns are:
   * <ul>
   * <li><b>?</b> matches any single character, except <b>.</b></li>
   * <li><b>&ast;</b> matches any sequence of characters (including empty string), except
   * <b>.</b></li>
   * <li><b>&ast;&ast;</b> matches any sequence of characters (including empty string), including
   * <b>.</b></li>
   * </ul>
   */
  private String[] warnThresholds;


  @Valid
  @NestedConfigurationProperty
  private final Formatter formatter = new Formatter();


  public boolean isEnabled()
  {
    return enabled;
  }

  public void setEnabled(boolean enabled)
  {
    this.enabled = enabled;
  }

  public String getLoggerName()
  {
    return loggerName;
  }

  public void setLoggerName(String loggerName)
  {
    this.loggerName = loggerName;
  }

  public boolean isGapsTraced()
  {
    return gapsTraced;
  }

  public void setGapsTraced(boolean gapsIncluded)
  {
    this.gapsTraced = gapsIncluded;
  }

  public Duration getGapsThreshold()
  {
    return gapsThreshold;
  }

  public void setGapsThreshold(Duration gapsThreshold)
  {
    this.gapsThreshold = gapsThreshold;
  }

  public String[] getIncludes()
  {
    return includes;
  }

  public void setIncludes(String[] filters)
  {
    this.includes = filters;
  }

  public String[] getExcludes()
  {
    return excludes;
  }

  public void setExcludes(String[] filters)
  {
    this.excludes = filters;
  }

  public String[] getWarnThresholds()
  {
    return warnThresholds;
  }

  public void setWarnThresholds(String[] warnThresholds)
  {
    this.warnThresholds = warnThresholds;
  }

  public Formatter getFormatter()
  {
    return formatter;
  }


  public static class Formatter
  {

    /**
     * Minimum line width used by the trace formatter.
     */
    @PositiveOrZero
    private int lineWidth = 0;

    /**
     * Ensures that the actual trace log starts on a new line.
     */
    private boolean prefixNewLine = true;

    /**
     * The style used for the hierarchical representation of the traces.
     */
    @NotNull
    private TreeStyle treeStyle = TreeStyle.SPACES;


    public int getLineWidth()
    {
      return lineWidth;
    }

    public void setLineWidth(int lineWidth)
    {
      this.lineWidth = lineWidth;
    }

    public boolean getPrefixNewLine()
    {
      return prefixNewLine;
    }

    public void setPrefixNewLine(boolean prefixNewLine)
    {
      this.prefixNewLine = prefixNewLine;
    }

    public TreeStyle getTreeStyle()
    {
      return treeStyle;
    }

    public void setTreeStyle(TreeStyle treeStyle)
    {
      this.treeStyle = treeStyle;
    }

  }

}
