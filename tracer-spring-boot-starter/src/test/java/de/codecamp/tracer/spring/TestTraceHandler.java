package de.codecamp.tracer.spring;


import static org.assertj.core.api.Assertions.assertThat;

import de.codecamp.tracer.Trace;
import de.codecamp.tracer.TraceHandler;
import org.assertj.core.api.ObjectAssert;


public class TestTraceHandler
  implements
    TraceHandler
{

  private static Trace trace;


  public static Trace getTrace()
  {
    return trace;
  }

  public static ObjectAssert<Trace> assertThatTrace()
  {
    return assertThat(trace);
  }

  public static void clearTrace()
  {
    trace = null;
  }


  @Override
  public void handle(Trace trace)
  {
    TestTraceHandler.trace = trace;
  }

}
