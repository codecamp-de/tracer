package de.codecamp.tracer.spring;


import static org.assertj.core.api.Assertions.assertThat;

import de.codecamp.tracer.ActiveTrace;
import de.codecamp.tracer.TraceContext;
import de.codecamp.tracer.TraceHandler;
import de.codecamp.tracer.Tracer;
import java.time.Duration;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.test.context.ActiveProfiles;


@SpringBootTest
@ActiveProfiles("test")
@EnableAutoConfiguration
class TracerSpringBootIT
{

  @Autowired
  Tracer tracer;


  @AfterEach
  void cleanUp()
  {
    TestTraceHandler.clearTrace();
  }



  @Test
  void detectGap()
    throws InterruptedException
  {
    long gapDuration = 250;

    try (TraceContext tc = tracer.openContext(getClass().getCanonicalName(), "1"))
    {
      Thread.sleep(gapDuration);
      try (ActiveTrace t = tc.startTrace("1.1"))
      {
      }
      Thread.sleep(gapDuration);
      try (ActiveTrace t = tc.startTrace("1.2"))
      {
      }
      Thread.sleep(gapDuration);
    }

    assertThat(TestTraceHandler.getTrace()).isNotNull();
    assertThat(TestTraceHandler.getTrace().getLabel()).isEqualTo("1");

    // two (sub)traces and three gaps
    assertThat(TestTraceHandler.getTrace().getSubTraces()).hasSize(5);

    assertThat(TestTraceHandler.getTrace().getSubTraces().get(0).isGap()).isTrue();
    assertThat(TestTraceHandler.getTrace().getSubTraces().get(0).getDuration())
        .isGreaterThanOrEqualTo(Duration.ofMillis(gapDuration));

    assertThat(TestTraceHandler.getTrace().getSubTraces().get(1).isGap()).isFalse();

    assertThat(TestTraceHandler.getTrace().getSubTraces().get(2).isGap()).isTrue();
    assertThat(TestTraceHandler.getTrace().getSubTraces().get(2).getDuration())
        .isGreaterThanOrEqualTo(Duration.ofMillis(gapDuration));

    assertThat(TestTraceHandler.getTrace().getSubTraces().get(3).isGap()).isFalse();

    assertThat(TestTraceHandler.getTrace().getSubTraces().get(4).isGap()).isTrue();
    assertThat(TestTraceHandler.getTrace().getSubTraces().get(4).getDuration())
        .isGreaterThanOrEqualTo(Duration.ofMillis(gapDuration));
  }


  @Configuration
  @EnableAspectJAutoProxy(proxyTargetClass = true)
  static class Config
  {

    @Bean
    TraceHandler testTraceHandler()
    {
      return new TestTraceHandler();
    }

  }

}
